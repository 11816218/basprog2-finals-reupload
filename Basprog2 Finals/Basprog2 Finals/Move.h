#pragma once
#include <string>
#include <iostream>

using namespace std;

class Move
{
public:
	Move();
	Move(int _x, int _y);
	int location(int *x, int *y);
	bool isLocationSafe();
	void moving(int *x, int *y);
private:
	int x;
	int y;
	bool isSafe;
};