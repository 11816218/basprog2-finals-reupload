#pragma once
#include <string>
#include <time.h>
#include "Player.h"
#include <vector>
using namespace std;

class Pokemon
{
public:
	Pokemon();
	void setStarter(string _name, int _level, int _hp, int _baseHp, int _baseDamage, int _exp, int _expToNextLevel);
	Pokemon(string _playerName);
	void levelUp();
	void displayStats();
	void pokecenter();
	void startJourney();

private:
	int choice;
	string playerName;
	string name;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
};
//Pokedex
//Pokemon *treecko = new Pokemon("Treecko", 5, 59, 59, 19, 0, 39);
//Pokemon *torchic = new Pokemon("Torchic", 5, 59, 59, 19, 0, 39);
//Pokemon *mudkip = new Pokemon("Mudkip", 5, 59, 59, 19, 0, 39);
//Pokemon *zigzagoon = new Pokemon("Zigzagoon", 3, 25, 25, 5, 0, 28);
//Pokemon *wurmple = new Pokemon("Wurmple", 3, 19, 19, 3, 0, 28);
//Pokemon *silcoon = new Pokemon("Silcoon", 3, 30, 30, 0, 0, 28);
//Pokemon *cascoon = new Pokemon("Cascoon", 3, 30, 30, 0, 0, 28);
//Pokemon *poochyena = new Pokemon("Poochyena", 5, 35, 35, 10, 0, 39);
//Pokemon *seedot = new Pokemon("Seedot", 4, 29, 29, 7, 0, 33);
//Pokemon *lotad = new Pokemon("Lotad", 5, 27, 27, 9, 0, 39);
//Pokemon *ralts = new Pokemon("Ralts", 6, 31, 31, 11, 0, 44);
//Pokemon *taillow = new Pokemon("Taillow", 6, 33, 33, 13, 0, 44);
//Pokemon *wingull = new Pokemon("Wingull", 6, 28, 28, 10, 0, 44);
//Pokemon *shroomish = new Pokemon("Shroomish", 8, 35, 35, 18, 0, 51);
//Pokemon *slakoth = new Pokemon("Slakoth", 9, 40, 40, 18, 0, 55);