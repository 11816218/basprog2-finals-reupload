#include <iostream>
#include "Pokemon.h"
#include "Player.h"
#include "Move.h"
#include "Action.h"
#include <string>
#include <vector>
#include <time.h>

using namespace std;

void splash()
{
	cout << R"(
                                                                                                    
,-.----.                                                                                            
\    /  \                  ,-.                    ____                                              
|   :    \             ,--/ /|                  ,'  , `.                                            
|   |  .\ :   ,---.  ,--. :/ |               ,-+-,.' _ |   ,---.        ,---,                       
.   :  |: |  '   ,'\ :  : ' /             ,-+-. ;   , ||  '   ,'\   ,-+-. /  |                      
|   |   \ : /   /   ||  '  /      ,---.  ,--.'|'   |  || /   /   | ,--.'|'   |                      
|   : .   /.   ; ,. :'  |  :     /     \|   |  ,', |  |,.   ; ,. :|   |  ,"' |                      
;   | |`-' '   | |: :|  |   \   /    /  |   | /  | |--' '   | |: :|   | /  | |                      
|   | ;    '   | .; :'  : |. \ .    ' / |   : |  | ,    '   | .; :|   | |  | |                      
:   ' |    |   :    ||  | ' \ \'   ;   /|   : |  |/     |   :    ||   | |  |/                       
:   : :     \   \  / '  : |--' '   |  / |   | |`-'       \   \  / |   | |--'                        
|   | :      `----'  ;  |,'    |   :    |   ;/            `----'  |   |/                            
`---'.|              '--'       \   \  /'---'                     '---'                             
  `---`                          `----'                                                             
  .--.--.                      ____                 ,--,                  ___                       
 /  /    '.   ,--,           ,'  , `.             ,--.'|                ,--.'|_                     
|  :  /`. / ,--.'|        ,-+-,.' _ |        ,--, |  | :                |  | :,'   ,---.    __  ,-. 
;  |  |--`  |  |,      ,-+-. ;   , ||      ,'_ /| :  : '                :  : ' :  '   ,'\ ,' ,'/ /| 
|  :  ;_    `--'_     ,--.'|'   |  || .--. |  | : |  ' |     ,--.--.  .;__,'  /  /   /   |'  | |' | 
 \  \    `. ,' ,'|   |   |  ,', |  |,'_ /| :  . | '  | |    /       \ |  |   |  .   ; ,. :|  |   ,' 
  `----.   \'  | |   |   | /  | |--'|  ' | |  . . |  | :   .--.  .-. |:__,'| :  '   | |: :'  :  /   
  __ \  \  ||  | :   |   : |  | ,   |  | ' |  | | '  : |__  \__\/: . .  '  : |__'   | .; :|  | '    
 /  /`--'  /'  : |__ |   : |  |/    :  | : ;  ; | |  | '.'| ," .--.; |  |  | '.'|   :    |;  : |    
'--'.     / |  | '.'||   | |`-'     '  :  `--'   \;  :    ;/  /  ,.  |  ;  :    ;\   \  / |  , ;    
  `--'---'  ;  :    ;|   ;/         :  ,      .-./|  ,   /;  :   .'   \ |  ,   /  `----'   ---'     
            |  ,   / '---'           `--`----'     ---`-' |  ,     .-./  ---`-'                     
             ---`-'                                        `--`---'                                 
                                                                                                    
)" << '\n';
	cout << "				Vince Lauro G. Velasco III				" << endl;
	cout << "					BSIEMC - TG103A						" << endl;
	cout << "					BasProg2 Finals						" << endl;
	system("pause");
	system("cls");
};


int main()
{
	string name; //variable for Player.H

	Pokemon *player = new Pokemon(name);

	Player *playerName = new Player(name);

	Action *action = new Action();
	//Start of the program hereafter
	splash();
	playerName->whatsYourName(name); //Naming of Trainer

	Pokemon *myStarter = new Pokemon();
	cout << "I have 3 Pokemons on hand right now." << endl << "You can have one and let it be your partner for your journey! Go on, choose!" << endl << endl;
	cout << "[1] Treecko - Level 5" << endl;
	cout << "[2] Torchic - Level 5" << endl;
	cout << "[3] Mudkip - Level 5" << endl;

	int choice = 0;
	cin >> choice;
	switch (choice)
	{
	case 1:
		myStarter->setStarter("Treecko", 5, 59, 59, 19, 0, 39);
		break;
	case 2:
		myStarter->setStarter("Torchic", 5, 59, 59, 19, 0, 39);
		break;
	case 3:
		myStarter->setStarter("Mudkip", 5, 59, 59, 19, 0, 39);
		break;
	}

	player->startJourney();
	action->mainAction(); // Main action of simulator; Move or check pokemon
	system("cls");
}
