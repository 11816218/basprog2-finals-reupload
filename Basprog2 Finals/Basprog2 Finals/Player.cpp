#include "Player.h"
#include "Pokemon.h"
#include <iostream>

using namespace std;


Player::Player()
{
	playerName = "";
}

Player::Player(string _playerName)
{
	playerName = _playerName;
}


string Player::whatsYourName(string _playerName)
{
	cout << "Welcome to the World of Pokemon! What is your name?" << endl;
	cin >> _playerName;
	cout << "So your name is " << _playerName << "? Correct? Excellent!" << endl;
	system("pause");
	system("cls");
	return string(_playerName);
}