#include "Move.h"
#include "Action.h"

Move::Move()
{
	x = 0;
	y = 0;
	isSafe = true;
}

Move::Move(int _x, int _y)
{
	x = _x;
	y = _y;
}

int Move::location(int * x, int * y)
{
	cout << "You are now at position (" << *x << ", " << *y << ")" << endl;
	while (true)
	{
		if (*x == 0 && *y == 0 || *x <= 2 && *y <= 2 && *x >= -2 && *y >= -2)
		{
			cout << "You are in Littleroot Town" << endl;
			isSafe = true;
			isLocationSafe();
			system("pause");
			system("cls");
		}
		else if (*x <= 5 && *y <= 5 && *x >= 3 && *y >= 3)
		{
			cout << "You are in Route 101" << endl;
			isSafe = false;
			isLocationSafe();
			system("pause");
			system("cls");
		}
		else if (*x <= 9 && *y <= 9 && *x >= 6 && *y >= 6)
		{
			cout << "You are in Petalburg City" << endl;
			isSafe = true;
			isLocationSafe();
			system("pause");
			system("cls");
		}
		else if (*x <= 12 && *y <= 12 && *x >= 10 && *y >= 10)
		{
			cout << "You are in Petalburg Woods" << endl;
			isSafe = false;
			isLocationSafe();
			system("pause");
			system("cls");
		}
		else
		{
			cout << "You are in an unknown location" << endl;
			isSafe = false;
			isLocationSafe();
			system("pause");
			system("cls");
		}
		Action *choiceAction = new Action();
		choiceAction->mainAction();
		return this->x, this->y;
	}
}

bool Move::isLocationSafe()
{
	if (isSafe == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void Move::moving(int * x, int * y)
{
	/*Action *choiceAction = new Action();
	choiceAction->mainAction();*/
	while (true)
	{
		char direction;
		cout << "Where do you want to go?" << endl;
		cout << "[W] - Up" << "		" << "[S] - Down" << "		" << "[A] - Left" << "		" << "[D] - Right" << endl;
		cin >> direction;
		switch (direction)
		{
		case 'W':
		case 'w':
			*y += 1;
			break;
		case 'S':
		case 's':
			*y -= 1;
			break;
		case 'A':
		case 'a':
			*x -= 1;
			break;
		case 'D':
		case 'd':
			*x += 1;
			break;
		}
		location(x, y);
	}
	system("pause");
	system("cls");
}