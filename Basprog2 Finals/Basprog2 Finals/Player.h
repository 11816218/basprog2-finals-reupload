#pragma once
#include <string>

using namespace std;

class Player
{
public:
	Player();
	Player(string _playerName);
	string whatsYourName(string _playerName);
private:
	string playerName;
};