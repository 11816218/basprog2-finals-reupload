#include "Pokemon.h"
#include "Player.h"
#include <iostream>
#include <vector>
#include <time.h>

using namespace std;
Pokemon::Pokemon()
{
	name = "";
	level = 5;
	hp = 39;
	baseHp = 39;
	baseDamage = 15;
	exp = 0;
	expToNextLevel = 38;
	playerName = "";
}

void Pokemon::setStarter(string _name, int _level, int _hp, int _baseHp, int _baseDamage, int _exp, int _expToNextLevel)
{
	name = _name;
	level = _level;
	hp = _hp;
	baseHp = _baseHp;
	baseDamage = _baseDamage;
	exp = _exp;
	expToNextLevel = _expToNextLevel;
}

Pokemon::Pokemon(string _playerName)
{
	playerName = _playerName;
}



void Pokemon::levelUp()
{
	if (exp == expToNextLevel)
	{
		this->level = ((level * .15) + this->level);
		this->baseDamage = ((baseDamage * .10) + this->baseDamage);
		this->expToNextLevel = ((expToNextLevel * .20) + this->expToNextLevel);
		this->exp = ((exp *.20) + this->exp);
		exp == 0;
	}
}

void Pokemon::displayStats()
{
	cout << name << " stats:" << endl;
	cout << "Level: " << level << endl;
	cout << "Health Points: " << hp << "/" << baseHp << endl;
	cout << "Damage: " << baseDamage << endl;
	cout << "Exp: " << exp << "/" << expToNextLevel << endl << endl;
	system("pause");
}

void Pokemon::pokecenter()
{
	char choice;
	cout << "Welcome to the Pokemon Center!" << endl;
	cout << "Would you like to heal your Pokemon back to perfect health? (y/n)" << endl;
	cin >> choice;
	switch (choice)
	{
	case 'Y':
	case 'y':
		system("cls");
		cout << "*ten ten tenenenen*" << endl;
		system("pause");
		cout << "Your Pokemons are now in perfect health!" << endl << endl;
		this->hp == baseHp;
		displayStats();
		cout << "We hope to see you again soon!" << endl;
		system("pause");
		system("cls");
		break;
	case 'N':
	case 'n':
		system("pause");
		cout << "We hope to see you again soon!" << endl;
		system("pause");
		system("cls");
		break;
	}
}

void Pokemon::startJourney()
{
	cout << "Your journey begins now!" << endl;
	system("pause");
	system("cls");
}


