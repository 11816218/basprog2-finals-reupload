#pragma once

class Action
{
public:
	Action();
	Action(int _mainChoice);
	void mainAction();
private:
	int mainChoice;
};